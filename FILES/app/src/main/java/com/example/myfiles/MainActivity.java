package com.example.myfiles;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class MainActivity extends AppCompatActivity {


    private EditText from;
    private EditText to;
    private ListView view;


    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        from = (EditText) findViewById(R.id.editFrom);
        to = (EditText) findViewById(R.id.editTo);
        String[] king = getResources().getStringArray(R.array.Kings);
        ListView view = (ListView) findViewById(R.id.lstKings);
        List<King> list = new ArrayList<>();

        try {
            InputStream stream = getResources().openRawResource(R.raw.kings);
            DocumentBuilder builder =
                    DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.parse(stream, null);

            NodeList nodes = document.getElementsByTagName("kings");

            for (int i = 0; i < nodes.getLength(); i++) {
                Element elem = (Element) nodes.item(i);
                String attr1 = elem.getAttribute("name");
                String attr2 = elem.getAttribute("from");
                String attr3 = elem.getAttribute("to");
                String[][] data = {
                        {attr1, attr2, attr3}
                };
                for (String[] arr : data) {
                    list.add(new King(arr[0], Integer.parseInt(arr[1]), Integer.parseInt(arr[2]), arr[3]));
                }
            }

            stream.close();
        } catch (Exception e) {
        }
        view.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, king));
        view.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        int a=list.get(position).getFrom();
                        int b=list.get(position).getTo();
                        from.setText(""+a);
                        to.setText(""+b);

                    }
                });
    }

}