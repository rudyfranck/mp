package com.example.constraintlayout;

import java.util.ArrayList;
import java.util.List;

public class Kings {
    private List<King> list = new ArrayList<>();

    public Kings()
    {
        for (String[] arr : data)
        {
            list.add(new King(arr[0],
                    Integer.parseInt(arr[1]),
                    Integer.parseInt(arr[2])));
        }
    }

    public List<King> getKings() {
        return list;
    }

    private static final String[][] data = {
            {"rudy", "1995", "2220"},
            {"Franck", "1996", "3920"},
            {"lee", "1994", "1026"},
            {"arnaud ", "1993", "1021"},
            {"rodrique", "1872", "1883"}
    };
}
