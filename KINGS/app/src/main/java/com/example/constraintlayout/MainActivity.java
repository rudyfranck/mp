package com.example.constraintlayout;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<King> kings = (new Kings()).getKings();
    private EditText from;
    private EditText to;
    private ListView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        from = (EditText) findViewById(R.id.editFrom);
        to = (EditText) findViewById(R.id.editTo);
        view = (ListView) findViewById(R.id.lstKings);
        view.setAdapter(new ArrayAdapter<King>(this,
                android.R.layout.simple_list_item_1, kings));
        
        view.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        update(position);
                    }
                }
        );
    }

    private void update(int position) {
        int a = kings.get(position).getFrom();
        int b = kings.get(position).getTo();
        from.setText("" + a);
        to.setText("" + b);
    }

    private void disable(EditText from) {
        from.setKeyListener(null);
        from.setEnabled(false);
    }


}