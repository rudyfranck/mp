package com.example.project2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.ListView

class MainActivity : AppCompatActivity(),View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
         findViewById<Button>(R.id.button1).setOnClickListener(this)
        findViewById<Button>(R.id.button2).setOnClickListener(this)
        Zipcodes.init()
    }


    override fun onClick(view: View) {


        val text1 : EditText = findViewById(R.id.editText1)
        val text2 : EditText = findViewById(R.id.editText2)
        if (view.id == R.id.button1) {
            text1.setText("")
            text2.setText("")
        }
        else if (view.id == R.id.button2) {
            val list : List<Zipcode> = Zipcodes.search(text1.text.toString().trim(), text2.text.toString().trim(), packageName)


            Log.e(packageName, list.size.toString())

            val adapter : ArrayAdapter<Zipcode> = ArrayAdapter<Zipcode>(this, android.R.layout.simple_list_item_1, list)
            val lv : ListView = findViewById(R.id.listView1)
            lv.adapter = adapter
        }

    }
}
