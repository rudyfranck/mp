package com.example.project2;


import java.util.ArrayList;
import java.util.List;

public class Zipcodes {
    private static List<Zipcode> list = new ArrayList();

    public static void init() {
        for (int i = 0; i < codes.length; i++)
            list.add(new Zipcode(codes[i][0], codes[i][1]));
    }



    public static List<Zipcode> search(String code, String city, String pac) {
        city = city.toLowerCase();
        List<Zipcode> lines = new ArrayList();

        for (Zipcode Z : list)
            if (Z.getCode().startsWith(code) && Z.getCity().toLowerCase().contains(city))
                lines.add(Z);
        return lines;


    }

    private static String[][] codes = {
            {"603000", "nizhny novgorod"},
            {"603001", "nizhny novgorod"},
    };
}
