package com.example.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText info;
    String pass;
    Button btn;
    TextView display2;
    String pass4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Activity 1");
        info=(EditText)findViewById(R.id.edit1);
        btn=findViewById(R.id.btn_start);
        display2= findViewById(R.id.act3return);

        btn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                //Intent intent=new Intent(MainActivity.this,MainActivity2.class);
                Intent intent2 =new Intent(MainActivity.this,MainActivity3.class);

                //pass=info.getText().toString();
                //intent.putExtra("pass",pass);
                //startActivity(intent);
                startActivityForResult(intent2, 1);
                finish();
            }
        });}


        @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                String strEditText = data.getStringExtra("pass3");
                display2.setText(strEditText);

                if (requestCode == RESULT_CANCELED) {
                    display2.setText("Nothing was typed in Act 3");
                }


            }
        }}}


