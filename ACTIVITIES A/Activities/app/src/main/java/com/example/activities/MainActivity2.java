package com.example.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {
    TextView display1;
    String pass2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        setTitle("Activity 2");
        display1= findViewById(R.id.A1display);
        pass2=getIntent().getStringExtra("pass");
        display1.setText(pass2);


    }
    public void closeActivity(View view){
        finish();
    }
    }
