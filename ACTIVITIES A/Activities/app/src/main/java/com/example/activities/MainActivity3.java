package com.example.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity3 extends AppCompatActivity {
    EditText info3;
    String pass3;
    Button btn3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        setTitle("Activity 3");
        btn3=findViewById(R.id.button2);
        info3=(EditText)findViewById(R.id.act3);
        Intent intent4 =getIntent();
        intent4.putExtra("pass3","yaho!!!");
        setResult(RESULT_OK, intent4);
        finish();

    }


    //    public void act3click(View view) {
//        pass3=info3.getText().toString();

//        startActivity(intent4);

//
//    }

    public void close(View view) {finish();
    }
}