package com.example.constraints;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity{
    public static final int MENU_NAME= Menu.FIRST+1;
    public static final int MENU_TEXT= Menu.FIRST+2;
    public static final int MENU_REMOVE= Menu.FIRST+3;
    public static final int MENU_RESET= Menu.FIRST+4;
    private List<King>kings=(new Kings()).getKings();
    private EditText from;
    private EditText to;
    private ListView view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        from=(EditText)findViewById(R.id.editFrom);
        to=(EditText)findViewById(R.id.editTo);
        view=(ListView)findViewById(R.id.lstKings);
        disable(from);
        disable(to);
        view.setAdapter(new ArrayAdapter<King>(this, android.R.layout.simple_list_item_1,kings));
        view.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                       update(position);
                    }
                }
        );
        registerForContextMenu(view);//attaching a context menu to list view
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,ContextMenu.ContextMenuInfo menuInfo)
    {
        menu.add(Menu.NONE,MENU_NAME, Menu.NONE,"Kings");
        menu.add(Menu.NONE,MENU_TEXT,Menu.NONE,"Description");
        menu.add(Menu.NONE,MENU_REMOVE,Menu.NONE,"Remove");
//        menu.add(Menu.NONE,MENU_RESET,Menu.NONE,"Reset");
    }

    @Override
    public boolean onContextItemSelected( MenuItem item) {
        AdapterView.AdapterContextMenuInfo menuInfo=(AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        King king=kings.get(menuInfo.position);
        switch (item.getItemId())
        {
            case MENU_NAME:
                Toast.makeText(this,king.toStringEx(),Toast.LENGTH_LONG).show();
                return true;
            case MENU_TEXT:
                AlertDialog.Builder builder =new AlertDialog.Builder(this);
                builder.setTitle(king.getName());
                builder.setMessage(king.getText());
                builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                     //
                    }
                });
                builder.show();
            case  MENU_REMOVE:
                remove();
                return true;
        }

        return super.onContextItemSelected(item);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        menu.add(Menu.NONE,MENU_RESET,Menu.NONE,"Reset");
        menu.add(Menu.NONE,MENU_REMOVE,Menu.NONE,"Remove");
        return (super.onCreateOptionsMenu(menu));
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case MENU_RESET:
                reset();
                return (true);
            case  MENU_REMOVE:
                remove();
                return (true);
        }
        return (super.onOptionsItemSelected(item));
    }

    private void remove() {
        kings=(new Kings()).getKings();
        view.setAdapter(new ArrayAdapter<King>(this, android.R.layout.simple_list_item_multiple_choice,kings));
        view.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                kings.remove(position);

            }});
    }

    private void reset() {
        kings.clear();



    }

    private void update(int position) {
        int a=kings.get(position).getFrom();
        int b=kings.get(position).getTo();
        from.setText(""+a);
        to.setText(""+b);

    }

    private void disable(EditText from){
        from.setKeyListener(null);
        from.setEnabled(false);
    }
}